# Makie Plot of the continuous Game of Life

#= Usage:
* add julia packages GLMakie, Images, Dates, FFTW
* change to cGoL_pks directory OR fill this path:   =#
path="/home/PATHREQUIRED/cGoL_pks"

#= TODO: 
* Graphical functions
* Optimize features check / update (lift? on? ...)
* implement different measures / observables
* tune λ-dependence for 1D and 3D (and 2D?)
=#

using GLMakie, Images, FFTW
FFTW.set_num_threads(16) 

dim=2  # dimension 1, 2 or 3

function cGoL(dim)

# Set image parameters
dim==3 ? n=[1 1 1]*2^7 : (dim==2 ? n=[2 1]*2^9 : n=[1]*2^14)

nstep=[3 30];  nt, nx = nstep
CHEMICAL=false;
λ=3; mconv, nconv=get_conv(n,nx,λ,CHEMICAL); # Gaussian or Bessel

asym=2 .^(0:0.3:1); asym=asym[1:dim]
#dim==3 ? asym=[1.2, 0.6, 1.6] : asym=[1] # sd asymmetry
#asym=ones(dim) # no asymmetry


kernel=Observable(CHEMICAL)
imind=Observable(2)

L=zeros(n...) #; M=copy(L); N=copy(L); G=copy(L)
go=[]
if dim==2
    gameover=copy(RGB.(L))
    try
    go=load("./plots/game_over.png")
    catch
        try
            go=load(path*"/plots/game_over.png")
        catch
            display("Please fill path to cGoL_pks folder")
        end
    end
    iim=(size(L').-size(go)).÷2
    gameover[iim[2]:iim[2]+size(go,2)-1,iim[1]+size(go,1)-1:-1:iim[1]]=go'
    L[1]=1.
    Li=Observable(RGB.(L))
elseif dim==3
    L[1]=1.
    Li=Observable(L)
elseif dim==1
    L[1]=1.
    Li=Observable(RGB.([L L]));
end
#display(gameover)

_, k=radial_spectrum(L,5,0)


x=(0:n[1]-1)/nx
xrange=Observable((0,x[end]))

Lj=Observable(RGB.(zeros(101,101)))
Mj=Observable(L[1:10:end]) #Int(ceil(nx^2/10))
Nj=Observable(L[1:10:end])
Cj=Observable(L[1:10:end])

xm=Observable([0., 0., 0.])

# fig = Figure(backgroundcolor = :darkgray, resolution=(900 * (sqrt(5) / 2 - 1 / 2), 900))
fig = GLMakie.Figure(backgroundcolor = RGBf(0.5, 0.5, 0.5), size=(1600,1300))
 
if dim==2
    y=(0:n[2]-1)/nx
    yrange=Observable((0,y[end]))
    ax, ima=image(fig[1,2:4][1,1:3],xrange,yrange,Li; colorrange = (0, 1),axis = (; aspect = DataAspect(),))  # image
elseif dim==1
#    y=Observable(0:1)
    ax, ima=image(fig[1,2:4][1,1:3],xrange,(0,1),Li; colorrange = (0, 1))  # image
elseif dim==3
    y=(0:n[2]-1)/nx
    z=(0:n[3]-1)/nx
    ax = Axis3(fig[1,2:4][1,1:3], aspect=:data, perspectiveness=0.4)#, viewmode=:fit)
    volume!(ax,x,y,z,Li, colorrange = (0, 1), algorithm = :mip)#, transparency=true)#,colormap=:turbo)#,colormap=:grays)
    #hidedecorations!(ax)
    hidespines!(ax)
    ax.ylabel=""; ax.zlabel=""
end

bx, img2=image(fig[1,1][1,1],(0, 1),(0, 1),Lj)#; axis = (; aspect = DataAspect(),)) #; colorrange = (0, 1))  # image
#xlims!(bx,(0,1))
#cm1=resample_cmap(:hsv, 360)
scatter!(bx,Nj,Mj, markersize=5, alpha=0.2)#, alpha = Cj, colormap=:hawaii) #colorrange=-0.5:0.1:0.5, color=Mj,  color=Cj, colorrange = (-0.1, 0.1), colormap = cm1, color=Cj, colorrange = (0, 1) , marker=Circle 
scatter!(bx,xm,xm,markersize=[4,4,3])
# x=0:0.0001:1; lines(x,rule0(x,x,param).-x)


cx=GLMakie.Axis(fig[1,1][6,1],xscale=log10,yscale=log10)

#lines!(cx,k,S)
xlims!(cx,(k[1],k[end]))
ylims!(cx,(1e2,1e8))

ax.title="cGoL Lab"
bx.title="Target function G(M,N)"
bx.xlabel="N"
bx.ylabel="M"
cx.title="Radial power spectrum"


#on(ax.scene.events.mousebutton) do # not working
#    display(ispressed(ax,Mouse.left))
#end

# Buttons
a1, a2, a3, a4, a5 = Observable(true), Observable(false), Observable(true), Observable(false), Observable(false)

fig[1,1][3, 1] = buttongrid = GridLayout(tellwidth = false)

btn = buttongrid[1,1:4] = [
Button(fig, label = @lift($a1 ? "LIFE" : "GAME")),
Button(fig, label = @lift($a2 ? "PLAY" : "PAUSE")),
Button(fig, label = "RESET"),
Button(fig, label = "STOP")]
for (ia,action) in enumerate([a1, a2, a3, a4])
    on(btn[ia].clicks) do _
        action[]=!action[]
    end
end

# Sliders

sl1 = SliderGrid(fig[1,1][2,1],
(label="dt", range=2 .^(-5:0.01:0), startvalue = 1/nt),
(label="dx", range=2 .^(-7:0.01:0), startvalue = 1/nx),
(label="spot radius", range=0:0.1:10, startvalue = 2)
) # numerical parameters
dt = sl1.sliders[1].value
dx = sl1.sliders[2].value
radius = sl1.sliders[3].value


# Dividing cells for λ=3
# param1=[0.55, 0.06, 0.395, 0.055, 0.038, 0.34]
# param2=[0.5, 0.11, 0.295, 0.059, 0.015, 0.236]
# param3=[0.38, 0.1, 0.13, 0.03, 0.004, 0.11] 
par=[0.765 0.697  0.528; 0.170 0.31 0.283; 1.12 0.833 0.368; 0.343 0.38 0.187; 0.342 0.41 0.324; 0.572 0.395 0.185]

sl2 = SliderGrid(fig[1,2:4][2,1:2],
(label="λ", range=1.5:0.01:10, startvalue = 3.),
(label="Mc", range=(0.9:0.0001:1.1)*par[1,dim], startvalue = par[1,dim]),# range=0.45:0.001:0.55, startvalue = 0.5),
(label="ΔMc", range=0:0.001:1, startvalue = par[2,dim]),
(label="Nc", range=0:0.001:1.2, startvalue = par[3,dim]),
(label="ΔNc", range=0:0.001:1, startvalue = par[4,dim]), #167), #0.1025
(label="ΔN0", range=0:0.001:1, startvalue = par[5,dim]),#0.238),
(label="ΔN1", range=0:0.001:1, startvalue = par[6,dim]), #667)
)
sl3 = SliderGrid(fig[1,2:4][2,3],
(label="", range=0:0, startvalue = 0.),
(label="", range=-1:0.01:1, startvalue = 0.),# range=0.45:0.001:0.55, startvalue = 0.5),
(label="", range=-1:0.01:1, startvalue = 0.),
(label="", range=-1:0.01:1, startvalue = 0.),
(label="", range=-1:0.01:1, startvalue = 0.), #167), #0.1025
(label="", range=-1:0.01:1, startvalue = 0.),#0.238),
(label="", range=-1:0.01:1, startvalue = 0.), #667)
) # game parameters
Λ = sl2.sliders[1].value
pr = [sl2.sliders[v].value for v=2:7]
gr = [sl3.sliders[v].value for v=2:7] # gradients

menu = Menu(fig[1,1][4,1], options = zip(["Gaussian kernel", "Bessel kernel (~exponential): diffusion steady state"],[false, true]))
on(menu.selection) do s
    kernel[]=s
end
menu2 = Menu(fig[1,1][5,1], options = zip(["G: target","L: life", "M: cell", "N: neighbourhood","G-L: change (+ white, - black)", "hsv = N, M, L"], 1:6), default=2) #, "rgb = N, M, L", "rgb = L, N, M", "rgb = L, M, N", "rgb = M, L, N", "rgb = M, N, L"
on(menu2.selection) do s
    imind[]=s
end
function colouring(s,G,L,M,N,param)
    if s==1
        img=G
    elseif s==2
        img=L
    elseif s==3
        img=M
    elseif s==4
        img=N
    elseif s==5
        img=G-L.+1/2
    end

    if s==6 && length(size(L))<=2
        img=HSV.(mod.((N.-(param[3]-param[4]+param[5]))/(param[4]+param[5]+param[6]*2/3).-1/6,1)*360,M,L)
        if length(size(L))==1
            img=[img img]
        end
    elseif length(size(L))==2
        img=RGB.(img)
    elseif length(size(L))==1
        img=RGB.([img img])
    elseif s==6 && length(size(L))==3
        return L
    end
    return img
end
#menu3 = Menu(fig[1,1][6,1], options = zip(["G: target","L: life", "M: cell", "N: neighbourhood","G-L: change (+ white, - black)", "hsv = N, M, L", "6"], 1:6), default=2) 

t = -1.
tind, FPS = 0, 0
date=time()
while t<Inf
    if t<0; display(fig); a4[]=false; end # open figure
    param=[pr[1][]/Λ[]^(0.3), pr[2][]/sqrt(Λ[]^2-1), pr[3][]/sqrt(Λ[]^2-1), pr[4][]/Λ[]^(5/3), pr[5][]/Λ[]^(1+dim), pr[6][]/(Λ[]^2-1)^(1/4)]
    grad=[gr[v][] for v=1:6]

    if  (λ!=Λ[]) | (dx[]*nx!=1) | (CHEMICAL!=kernel[]) #
        λ=Λ[]; nx=1/dx[];         CHEMICAL=kernel[]
        x=(0:n[1]-1)/nx; xrange[]=(0,x[end])
        if dim==2; y=(0:n[2]-1)/nx; yrange[]=(0,y[end]); end
        dim<=2 ? tightlimits!(ax) : z=(0:n[3]-1)/nx
        mconv, nconv=get_conv(n,nx,Λ[],CHEMICAL) # Gaussian or Bessel
    end

    #if ispressed(ax,Keyboard.tab)
    #    a2[]!=a2[]
    #end
    if a2[]     # Pause
        sleep(0.3)
    elseif a4[] # Stop
        break
    elseif (sum(L)/prod(n)<100*eps(1.f0)) && dim==2 && tind>0 # Game Over + Reset
        Li[]=gameover
        sleep(1.0)
        L=float(green.(gameover))
        t=0.
    elseif a3[] || (sum(L)/prod(n)<100*eps(1.f0))    # Reset
        if ispressed(ax,Keyboard.left_control) #SEED
            L=Float32.(init0(n,asym*radius[]*nx,n/2))
        else
            L=Float32.(init(n,1000*Λ[]*nx,nconv))
        end
        a3[]=!a3[]
        t=0.
    else        # Play
        #Threads.@spawn 
        N=nconv(L)   
        M=mconv(L)
        G=rule0(M,N,param,grad)
        L.+=(G-L)*dt[]
        #L=lconv(L) ########### try to reduce pixel effect?
        t+=dt[]
        tind+=1

        if a1[] # Show rule/goal of the Game
            xm[]=fixed_pts(param,rule0)

            Lj[]=rule0((0:400)/400,(0:400)'/400,param)'
            Mj[]=M[1:10:end]#[1:Int(ceil(nx^2/10)):end]
            Nj[]=N[1:10:end]#[1:Int(ceil(nx^2/10)):end]
            Cj[]=mod.((N[1:10:end].-(param[3]-param[4]+param[5]))/(param[4]+param[5]+param[6]*2/3).-1/6,1)   #abs.(G[1:10:end]-L[1:10:end])
            if ispressed(cx,Keyboard.enter)#tind%100==0 # compute radial spectrum
                S,_=radial_spectrum(L,5,0)
                lines!(cx,k,S)
                #Slimits[]=(minimum(S[])/2,maximum(S[])*2)
            end

    #    else    # Show Life
            pos=mouseposition(ax.scene)
            pos2=mouseposition(bx.scene)
            # ispressed(ax,Mouse.left)
            if ispressed(ax,Keyboard.left_control) && pos[1]>=0 && pos[2]>=0 && dim<=2
                dim==2 ? L=spot(L,x,y,[pos[1], pos[2]],radius[]) : L=spot(L,x,pos[1],radius[]) 
            end
            pos2=mouseposition(bx.scene)
            if pos2[1]>=-0.1 && pos2[2]>=-0.1 && pos2[1]<=1.1 && pos2[2]<=1.1 && dim<=2
                rL=exp.(-((N.-pos2[1]).^2 .+(M.-pos2[2]).^2)/0.04^2)

                #rL[(N.>pos2[1]-0.02) .& (N.<pos2[1]+0.02) .& (M.>pos2[2]-0.02) .& (M.<pos2[2]+0.02)].*=-1
                if dim==1
                    Li[]=[RGB.(max.(rL,L),(1 .-rL).*L,(1 .-rL).*L) RGB.(max.(rL,L),(1 .-rL).*L,(1 .-rL).*L)]
                elseif dim==2
                    Li[]=RGB.(max.(rL,L),(1 .-rL).*L,(1 .-rL).*L)#RGB.(L,1 .-rL,1 .-rL)
                end
            else
                Li[]=colouring(imind[],G,L,M,N,param)
                dim==3 ? ax.azimuth[] += 2pi/360 : nothing   #1.7pi + 0.3 * sin(2pi * mod(tind,360) / 120) : nothing
            end
        else
            Li[]=colouring(imind[],G,L,M,N,param)
        end
    end 

    if mod(tind,50)==0
        date, FPS=time(), Int64(round(50/(time()-date)))#;digits=1)
    end
    tr=round(t;digits=1)
    ax.xlabel="Time: t = $tr s    ($FPS fps)"

    sleep(0.0000001) # prevents display bug on certain systems
end
end


#= *Precompute convolution-based smoothing functions that represent the "cell" and its "neighbourhood".*

# Inputs:
- sizes n (Array) of Life image L
- number of step nx (number) per space unit for space discretization
- ratio λ between "neighbourhood" (N) and "cell" (M) spatial scales
- chemical: if true, the kernel is a Bessel function (linked to reaction-diffusion system), and a Gaussian function if false.
- patient: if true, precompute extensively the possible ways of computing the FFTW (can be interesting for even longer simulations if the image sizes are unusual (not power of 2))

# Outputs:
- "cell" smoothing function M=mconv(L) 
- "neighbourhood" smoothing function N=nconv(L) 

# Usage:
mconv, nconv = get_conv([1440 1080],50,3)
M=mconv(L)
N=nconv(L)
=#
function get_conv(n,nx,λ,chemical=false,patient=false)   # precompute the 2 convolutions
    FFTW.set_num_threads(16) 
    if patient
        pr=plan_rfft(randn(Float32,n...); flags=FFTW.PATIENT,timelimit=Inf) # very slow to optimise a bit more
    else
        pr=plan_rfft(randn(Float32,n...); flags=FFTW.MEASURE,timelimit=Inf)
    end
    ipr=inv(pr)

    if chemical  # Cauchy filter / Bessel kernel as in a specific reaction-diffusion with source
        nr=copy(n); nr[1]=n[1]÷2+1; # for rfft

        rvec=FFTW.rfftfreq(n[1], nx).^2
        vec=zeros(nr...); vec.+=rvec
        if length(n)>1
            for l=eachindex(n[2:end])
                m=ones(Int,length(n)); m[l+1]=nr[l+1]
                vec.+=reshape(FFTW.fftfreq(n[l+1], nx).^2,m...)
            end
        end
        mfilter=1 ./(1 .+pi*vec)
        nfilter=1 ./(1 .+pi*vec*λ^2)
    else        # Gaussian filter / kernel
        mkernel=ones(n...)
        nkernel=ones(n...)
        for l=eachindex(n)
            m=ones(Int,length(n)); m[l]=n[l]
            mkernel.*=exp.(-pi*reshape(FFTW.fftfreq(n[l], n[l]/nx),m...).^2)
            nkernel.*=exp.(-pi*(reshape(FFTW.fftfreq(n[l], n[l]/(nx*λ)),m...)).^2)
        end
        mkernel/=sum(mkernel) 
        nkernel/=sum(nkernel)

        mfilter=real(pr*mkernel)
        nfilter=real(pr*nkernel)
    end
    
    mconv(L) = abs.(ipr*(mfilter.*(pr*L))) # what is the slow down due to abs.()? <-- not much
    nconv(L) = abs.(ipr*(nfilter.*(pr*L))) # abs.() is currently useful only for colouring purpose

    return mconv, nconv
end


#= *Initialize the Life L image*
# Uses a Gaussian window of a random binary pattern with the characteristic spatial scale of the "neighbourhood" N.
# WARNING: must be called after get_conv() (so that nconv() is defined).

# TODO / WARNING: an initialisation image may not be sufficient to seed cells, in 3D, a full protocol through parameter space seems required.

# Inputs:
- sizes n (Array) of Life image L
- sizes nseed (Array) in pixel of the seeded region

# # Output:
- Initial state L0 of the Life image 

# Usage:
L0=init([1440 1080],[100 100])
=#
function init(n,nseed,nconv)
    L=floor.(nconv(rand(n...))*2)
    window=ones(n...)
    for l=eachindex(n)
        m=ones(Int,3); m[l]=n[l]
        window.*=exp.(-pi*reshape(((1:n[l]).-n[l]÷2)/nseed,m...).^2)
    end
    L.*=window
end
function init0(n,nseed,pos)
    dim=length(n)
    if dim==1
        L=spot(zeros(n...),1:n[1],pos,nseed)
    elseif dim==2
        L=spot(zeros(n...),1:n[1],1:n[2],pos,nseed)
    elseif dim==3
        L=spot(zeros(n...),1:n[1],1:n[2],1:n[3],pos,nseed)
    end
    return L
end

## Interaction

#= *Add Gaussian spot to image*

# Inputs:
- 

# Output:
- image img

# Usage:

=#
function spot(img,x,pos,radius)
    X2=min.((x.-pos).^2,(x.-x[end].-x[2].-pos).^2,(x.+x[end].+x[2].-pos).^2)/radius[1]^2
    gausspot=exp.(-Float32(pi).*X2)
    img2=max.(img,gausspot)
    end
function spot(img,x,y,pos,radius)
    if length(radius)!=2
        radius=[1, 1]*radius
    end
    X2=min.((x.-pos[1]).^2,(x.-x[end].-x[2].-pos[1]).^2,(x.+x[end].+x[2].-pos[1]).^2)/radius[1]^2
    Y2=min.((y.-pos[2]).^2,(y.-y[end].-y[2].-pos[2]).^2,(y.+y[end].+y[2].-pos[2]).^2)/radius[2]^2
    gausspot=exp.(-Float32(pi).*(X2 .+ Y2'))
    img2=max.(img,gausspot)
end
function spot(img,x,y,z,pos,radius)
    if length(radius)!=3
        radius=[1, 1, 1]*radius[1]
    end
    X2=min.((x.-pos[1]).^2,(x.-x[end].-x[2].-pos[1]).^2,(x.+x[end].+x[2].-pos[1]).^2)/radius[1]^2
    Y2=min.((y.-pos[2]).^2,(y.-y[end].-y[2].-pos[2]).^2,(y.+y[end].+y[2].-pos[2]).^2)/radius[2]^2
    Z2=min.((z.-pos[3]).^2,(z.-z[end].-z[2].-pos[3]).^2,(z.+z[end].+z[2].-pos[3]).^2)/radius[3]^2
    gausspot=exp.(-Float32(pi).*(X2 .+ Y2' .+ reshape(Z2,1,1,length(Z2))))
    img2=max.(img,gausspot)
end

#= *Compute the set point / goal image G towards which the image evolves*

# Inputs:
- "cell" state M
- "neighbourhood" state N
- parameters p (matrix)

# # Output:
- goal state G
=#
function rule0(M,N,p) # p is simply a vector (rule1)
    #sigmoid(x, a, l) = (1+erf(sqrt(pi)*(x-a)/l))/2
    sigmoid(x, a, l) = 1/(1+exp((x-a)/l*4.4f0)) # simpler/faster!
    gauss(x, a, l) = exp(-Float32(pi).*((x-a)/l)^2)

    μ, σ = p[3]+p[4].-2*p[4]*sigmoid.(M,p[1],p[2]), p[6].+(p[5]-p[6])*sigmoid.(M,p[1],p[2])
    G=gauss.(N,μ,σ)
end

# rule with gradient
function rule0(M,N,param,grad) # param and grad are simply vectors (rule1)
    if sum(abs.(grad))==0
        return rule0(M,N,param)
    else
        sigmoid(x, a, l) = 1 ./(1 .+exp.((x.-a)./l*4.4f0))
        gauss(x, a, l) = exp(-Float32(pi).*((x-a)/l)^2)

        p=param.*(1 .-grad) #exp.(-grad)
        q=param.*(1 .+grad)

        lx=size(M,1)
        x=(0:lx-1)/(lx-1)
        pq=q'.*x .+p'.*(1 .-x)
        μ, σ = pq[:,3].+pq[:,4].-2*pq[:,4].*sigmoid.(M,pq[:,1],pq[:,2]), pq[:,6].+(pq[:,5]-pq[:,6]).*sigmoid.(M,pq[:,1],pq[:,2])

        G=gauss.(N,μ,σ)
    end
end


function fixed_pts(param,rule)
    x=0:1e-5:1
    F=rule(x,x,param).-x
    return [0, maximum(x[F.>eps(1.)]), minimum(x[F.>eps(1.)])]
end

#= *Compute the circular average of the power spectrum*

TODO: improve sampling

=#
function radial_spectrum(L,Q,dim)
    s=abs.(rfft(L)).^2
    n=size(L)
    nr=n.÷2 .+1
    ns=size(s)

    k0=zeros(ns)
    for l=eachindex(ns)
        m=ones(Int,length(ns)); m[l]=ns[l]
        k0.+=reshape(0:ns[l]-1,m...).^2
    end
    k0=sqrt.(k0);
    kmax=min(nr...);

    if dim==0
        k=exp.(0:0.1/Q:log(kmax))
        weight1(q::Matrix,Q)=exp.(-pi*(Q*log.(q)).^2)
        W=weight1(k0[k0.<=kmax]./k',Q)
    else
        k=(1:1/Q^dim:kmax^dim).^(1/dim)
        weight2(q::Matrix,Q)=exp.(-pi*(Q*q).^2)
        W=weight2(k0[k0.<=kmax].-k',Q)
    end
    S=W'*s[k0.<=kmax]./sum(W,dims=1)';
    return S[:,1], k
end


cGoL(dim)
