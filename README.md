# Introduction to the Continuous Game of Life

This is a selection of Julia scripts to try out the simulation of the following system:
$$
\begin{align}
\partial_t L(\vec{x},t)&=G(M(\vec{x},t),N(\vec{x},t))-L(\vec{x},t) \\
M(\vec{x},t)&=\phi_1\ast L(\vec{x},t) \\
N(\vec{x},t)&=\phi_\lambda\ast L(\vec{x},t) \quad ,
\end{align}
$$
where $\ast$ is a spatial convolution operation.

The dimensionality of space is changed by editing the `dim` variable (to 1, 2 or 3) in the script.

## cGoL_Makie_lab

Makie is a Julia library that allows to build UI and interact with the simulation. It relies on the successful installation of the package GLMakie.jl.

### Setup

From the Julia REPL, press "]" and install the packages:
`add GLMakie, Images, FFTW`

Then change directory to /home/.../cGoL_pks and run the script.

### Hidden interactivity

Apart from sliders, menus and buttons, we can interact with the mouse and the keyboard:
* Mouse position over the target function $G(M,N)$ shows corresponding locations in $L$.
* Mouse position over $L$ + `CTRL` key triggers an isotropic seed (with adjustable radius).
* `RESET` button + `CTRL` key reseeds $L$ with a single (non-isotropic) seed.
* `ENTER` computes the radial profile of the power spectrum of $L$.

## cGoL_minimal

This scripts runs without GLMakie. The first function `cGoL(dim)` contains the simulation loop and the different parameters.


## Reference
This work is based on:

* Cornus Ammonis' *Smoother Life*
https://www.shadertoy.com/view/XtVXzV

itself improving

* Stephan Rafler's *SmoothLife* https://arxiv.org/abs/1111.1567

Also have a look to the other variation of the model:

* Bert Chan's *Lenia* https://doi.org/10/ggf344