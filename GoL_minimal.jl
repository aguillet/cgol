# the continuous Game of Life
## minimal script

using FFTW, Images  # required packages (Images for vizualisation)
FFTW.set_num_threads(16) # can be changed to any number of CPU core 

dim=2


function cGoL(dim)
# Set image parameters

dim==3 ? n=[1 1 1]*2^7 : (dim==2 ? n=[1 1]*2^9 : n=[1]*2^14) # image size

nstep=[3 30];  nt, nx = nstep # small numerical scales (higher is smoother)
CHEMICAL=false;  # false-->Gaussian kernel, true-->Bessel (fast diffusion) kernel

λ=3; mconv, nconv=get_conv(n,nx,λ,CHEMICAL); # Gaussian or Bessel

param123=[0.55 0.5 0.38; 0.06  0.11 0.1; 0.395 0.295 0.13; 0.055 0.059 0.03; 0.038 0.015 0.004; 0.34 0.236 0.11]
param=param123[:,dim]  # suggestion of parameters for the target function

# Initialisation
radius=2.
asym=2 .^(0:0.3:1); asym=asym[1:dim]
#asym=ones(dim) # no asymmetry
L=Float32.(init0(n,asym*radius*nx,n/2))
# L=Float32.(init(n,1000*λ*nx,nconv)) # random initial pattern

# Simulation
T = 1000
t = 0.
tind=0
while t<T
    N=nconv(L)   
    M=mconv(L)
    G=rule0(M,N,param)
    L.+=(G-L)/nt
    t+=1/nt
    tind+=1
    mod(tind,5)==0 ? display(Gray.(L)) : nothing
end
end

## Functions

#= *Precompute convolution-based smoothing functions that represent the "cell" and its "neighbourhood".*

# Inputs:
- sizes n (Array) of Life image L
- number of step nx (number) per space unit for space discretization
- ratio λ between "neighbourhood" (N) and "cell" (M) spatial scales
- chemical: if true, the kernel is a Bessel function (linked to reaction-diffusion system), and a Gaussian function if false.
- patient: if true, precompute extensively the possible ways of computing the FFTW (can be interesting for even longer simulations if the image sizes are unusual (not power of 2))

# Outputs:
- "cell" smoothing function M=mconv(L) 
- "neighbourhood" smoothing function N=nconv(L) 

# Usage:
mconv, nconv = get_conv([1440 1080],50,3)
M=mconv(L)
N=nconv(L)
=#
function get_conv(n,nx,λ,chemical=false,patient=false)   # precompute the 2 convolutions
    FFTW.set_num_threads(16) 
    if patient
        pr=plan_rfft(randn(Float32,n...); flags=FFTW.PATIENT,timelimit=Inf) # very slow to optimise a bit more
    else
        pr=plan_rfft(randn(Float32,n...); flags=FFTW.MEASURE,timelimit=Inf)
    end
    ipr=inv(pr)

    if chemical  # Cauchy filter / Bessel kernel as in a specific reaction-diffusion with source
        nr=copy(n); nr[1]=n[1]÷2+1; # for rfft

        rvec=FFTW.rfftfreq(n[1], nx).^2
        vec=zeros(nr...); vec.+=rvec
        if length(n)>1
            for l=eachindex(n[2:end])
                m=ones(Int,length(n)); m[l+1]=nr[l+1]
                vec.+=reshape(FFTW.fftfreq(n[l+1], nx).^2,m...)
            end
        end
        mfilter=1 ./(1 .+pi*vec)
        nfilter=1 ./(1 .+pi*vec*λ^2)
    else        # Gaussian filter / kernel
        mkernel=ones(n...)
        nkernel=ones(n...)
        for l=eachindex(n)
            m=ones(Int,length(n)); m[l]=n[l]
            mkernel.*=exp.(-pi*reshape(FFTW.fftfreq(n[l], n[l]/nx),m...).^2)
            nkernel.*=exp.(-pi*(reshape(FFTW.fftfreq(n[l], n[l]/(nx*λ)),m...)).^2)
        end
        mkernel/=sum(mkernel) 
        nkernel/=sum(nkernel)

        mfilter=real(pr*mkernel)
        nfilter=real(pr*nkernel)
    end
    
    mconv(L) = abs.(ipr*(mfilter.*(pr*L))) # what is the slow down due to abs.()? <-- not much
    nconv(L) = abs.(ipr*(nfilter.*(pr*L))) # abs.() is currently useful only for colouring purpose

    return mconv, nconv
end


#= *Initialize the Life L image*
# Uses a Gaussian window of a random binary pattern with the characteristic spatial scale of the "neighbourhood" N.
# WARNING: must be called after get_conv() (so that nconv() is defined).

# TODO / WARNING: an initialisation image may not be sufficient to seed cells, in 3D, a full protocol through parameter space seems required.

# Inputs:
- sizes n (Array) of Life image L
- sizes nseed (Array) in pixel of the seeded region

# # Output:
- Initial state L0 of the Life image 

# Usage:
L0=init([1440 1080],[100 100])
=#
function init(n,nseed,nconv)
    L=floor.(nconv(rand(n...))*2)
    window=ones(n...)
    for l=eachindex(n)
        m=ones(Int,3); m[l]=n[l]
        window.*=exp.(-pi*reshape(((1:n[l]).-n[l]÷2)/nseed,m...).^2)
    end
    L.*=window
end
function init0(n,nseed,pos)
    dim=length(n)
    if dim==1
        L=spot(zeros(n...),1:n[1],pos,nseed)
    elseif dim==2
        L=spot(zeros(n...),1:n[1],1:n[2],pos,nseed)
    elseif dim==3
        L=spot(zeros(n...),1:n[1],1:n[2],1:n[3],pos,nseed)
    end
    return L
end

## Interaction

#= *Add Gaussian spot to image*

# Inputs:
- 

# Output:
- image img

# Usage:

=#
function spot(img,x,pos,radius)
    X2=min.((x.-pos).^2,(x.-x[end].-x[2].-pos).^2,(x.+x[end].+x[2].-pos).^2)/radius[1]^2
    gausspot=exp.(-Float32(pi).*X2)
    img2=max.(img,gausspot)
    end
function spot(img,x,y,pos,radius)
    if length(radius)!=2
        radius=[1, 1]*radius
    end
    X2=min.((x.-pos[1]).^2,(x.-x[end].-x[2].-pos[1]).^2,(x.+x[end].+x[2].-pos[1]).^2)/radius[1]^2
    Y2=min.((y.-pos[2]).^2,(y.-y[end].-y[2].-pos[2]).^2,(y.+y[end].+y[2].-pos[2]).^2)/radius[2]^2
    gausspot=exp.(-Float32(pi).*(X2 .+ Y2'))
    img2=max.(img,gausspot)
end
function spot(img,x,y,z,pos,radius)
    if length(radius)!=3
        radius=[1, 1, 1]*radius[1]
    end
    X2=min.((x.-pos[1]).^2,(x.-x[end].-x[2].-pos[1]).^2,(x.+x[end].+x[2].-pos[1]).^2)/radius[1]^2
    Y2=min.((y.-pos[2]).^2,(y.-y[end].-y[2].-pos[2]).^2,(y.+y[end].+y[2].-pos[2]).^2)/radius[2]^2
    Z2=min.((z.-pos[3]).^2,(z.-z[end].-z[2].-pos[3]).^2,(z.+z[end].+z[2].-pos[3]).^2)/radius[3]^2
    gausspot=exp.(-Float32(pi).*(X2 .+ Y2' .+ reshape(Z2,1,1,length(Z2))))
    img2=max.(img,gausspot)
end

#= *Compute the set point / goal image G towards which the image evolves*

# Inputs:
- "cell" state M
- "neighbourhood" state N
- parameters p (matrix)

# # Output:
- goal state G
=#
function rule0(M,N,p) # p is simply a vector (rule1)
    #sigmoid(x, a, l) = (1+erf(sqrt(pi)*(x-a)/l))/2
    sigmoid(x, a, l) = 1/(1+exp((x-a)/l*4.4f0)) # simpler/faster!
    gauss(x, a, l) = exp(-Float32(pi).*((x-a)/l)^2)

    μ, σ = p[3]+p[4].-2*p[4]*sigmoid.(M,p[1],p[2]), p[6].+(p[5]-p[6])*sigmoid.(M,p[1],p[2])
    G=gauss.(N,μ,σ)
end

cGoL(dim) # run simulation


